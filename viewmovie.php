<?php
    //1. เชื่อมต่อ database:
    include('connection.php');  //ไฟล์เชื่อมต่อกับ database ที่เราได้สร้างไว้ก่อนหน้าน้ี
    
    //2. query ข้อมูลจากตาราง:
    $query = "SELECT * FROM movie ORDER BY movie_id asc" or die("Error:" . mysqli_error());
    //3. execute the query.
    $result = mysqli_query($con, $query);
    //4 . แสดงข้อมูลที่ query ออกมา:
    
    //ใช้ตารางในการจัดข้อมูล
    echo "<table border='1' align='center' width='600'>";
    //หัวข้อตาราง
    echo "<tr align='center' bgcolor='#CCCCCC'><td>id</td><td>name</td><td>time</td><td>category</td><td>director</td><td>actor</td><td>language</td><td>แก้ไข</td><td>ลบ</td></tr>";
    while($row = mysqli_fetch_array($result)) {
        echo "<tr bgcolor='#CCCCCC'>";
        echo "<td>" .$row["movie_id"] .  "</td> ";
        echo "<td width='160'>" .$row["name"] .  "</td> ";
        echo "<td width='198'>" .$row["time"] .  "</td> ";
        echo "<td width='97'>" .$row["category"] .  "</td> ";
        echo "<td width='70'>" .$row["director"] .  "</td> ";
        echo "<td width='70'>" .$row["actor"] .  "</td> ";
		echo "<td width='70'>" .$row["language"] .  "</td> ";
       
        //แก้ไขข้อมูลส่่ง member_id ที่จะแก้ไขไปที่ฟอร์ม
        echo "<td><a href='userupdateform.php?cid=$row[0]'>edit</a></td> ";
        
        //ลบข้อมูล
        echo "<td><a href='UserDelete.php?cid=$row[0]' onclick=\"return confirm('Do you want to delete this record? !!!')\">del</a></td> ";
        echo "</tr>";
    }
    echo "</table>";
    //5. close connection
    mysqli_close($con);
    ?>                                                                   